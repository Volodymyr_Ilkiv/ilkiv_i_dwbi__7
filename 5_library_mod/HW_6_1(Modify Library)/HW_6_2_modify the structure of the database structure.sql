﻿use V_Ilkiv_Library
go

alter table V_Ilkiv_Library.dbo.Authors
add  
[birthday] date null,
[book_amount] int check ([book_amount]>=0),
[issue_amount] int check ([issue_amount]>=0),
[total_edition] int check ([total_edition]>=0) 
go

alter table V_Ilkiv_Library.dbo.Books
add 
[title] varchar(250) not null  default 'Title',
[edition] int not null default 1 check ([edition]>=1),
[published] date null ,
[issue] int  not null

go

ALTER TABLE Books ADD 
	title varchar(30) not NULL DEFAULT 'Title',
	edition int not null default (1) check (edition >= 1),
	published date null,
	issue int null;

--constraint uniq_issue unique (issue)

alter table V_Ilkiv_Library.dbo.Publishers
add  
created date not NULL DEFAULT ('1900-01-01'),
	country varchar(20) not null default ('USA'),
	city	varchar(20) not null default ('NY'),
	book_amount int not null default (0) check (book_amount >= 0),
	issue_amount int not null default (0) check (issue_amount>= 0),
	total_edition int not  null default (0) check (total_edition >= 0)
go

alter table V_Ilkiv_Library.dbo.Authors_log
add 
[book_amount_old] int null,
[issue_amount_old] int null,
[total_edition_old] int null,
[book_amount_new] int null,
[issue_amount_new] int null,
[total_edition_new] int null
go