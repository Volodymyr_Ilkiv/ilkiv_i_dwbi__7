﻿use V_Ilkiv_Library
go
---------------------------Update Authors
update V_Ilkiv_Library.dbo.Authors 
set [birthday] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[book_amount] = DEFAULT, 
	[issue_amount] = DEFAULT, 
	[total_edition] = DEFAULT
	where Author_Id=1 
go
update V_Ilkiv_Library.dbo.Authors 
set [birthday] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[book_amount] = DEFAULT, 
	[issue_amount] = DEFAULT, 
	[total_edition] = DEFAULT
	where Author_Id=2 
go
update V_Ilkiv_Library.dbo.Authors 
set [birthday] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[book_amount] = DEFAULT, 
	[issue_amount] = DEFAULT, 
	[total_edition] = DEFAULT
	where Author_Id=3 
go
update V_Ilkiv_Library.dbo.Authors 
set [birthday] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[book_amount] = DEFAULT, 
	[issue_amount] = DEFAULT, 
	[total_edition] = DEFAULT
	where Author_Id=4 
go
update V_Ilkiv_Library.dbo.Authors 
set [birthday] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[book_amount] = DEFAULT, 
	[issue_amount] = DEFAULT, 
	[total_edition] = DEFAULT
	where Author_Id=5 
go
update V_Ilkiv_Library.dbo.Authors 
set [birthday] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[book_amount] = DEFAULT, 
	[issue_amount] = DEFAULT, 
	[total_edition] = DEFAULT
	where Author_Id=6 
go
update V_Ilkiv_Library.dbo.Authors 
set [birthday] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[book_amount] = DEFAULT, 
	[issue_amount] = DEFAULT, 
	[total_edition] = DEFAULT
	where Author_Id=7 
go
update V_Ilkiv_Library.dbo.Authors 
set [birthday] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[book_amount] = DEFAULT, 
	[issue_amount] = DEFAULT, 
	[total_edition] = DEFAULT
	where Author_Id=8 
go
update V_Ilkiv_Library.dbo.Authors 
set [birthday] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[book_amount] = DEFAULT, 
	[issue_amount] = DEFAULT, 
	[total_edition] = DEFAULT
	where Author_Id=9 
go
update V_Ilkiv_Library.dbo.Authors 
set [birthday] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[book_amount] = DEFAULT, 
	[issue_amount] = DEFAULT, 
	[total_edition] = DEFAULT
	where Author_Id=10 
go
update V_Ilkiv_Library.dbo.Authors 
set [birthday] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[book_amount] = DEFAULT, 
	[issue_amount] = DEFAULT, 
	[total_edition] = DEFAULT
	where Author_Id=11 
go
update V_Ilkiv_Library.dbo.Authors 
set [birthday] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[book_amount] = DEFAULT, 
	[issue_amount] = DEFAULT, 
	[total_edition] = DEFAULT
	where Author_Id=12 
go
update V_Ilkiv_Library.dbo.Authors 
set [birthday] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[book_amount] = DEFAULT, 
	[issue_amount] = DEFAULT, 
	[total_edition] = DEFAULT
	where Author_Id=13 
go
update V_Ilkiv_Library.dbo.Authors 
set [birthday] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[book_amount] = DEFAULT, 
	[issue_amount] = DEFAULT, 
	[total_edition] = DEFAULT
	where Author_Id=14 
go


---------------------------Update Books
update V_Ilkiv_Library.dbo.Books  
set [title] = 'incredible title',
	[edition] = ABS(CHECKSUM(NEWID()) % 10000) + 70, 
	[published] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 10000), '1990-01-01'), 
	[issue] = ABS(CHECKSUM(NEWID()) % 5) + 1
	where V_Ilkiv_Library.dbo.Books.[Publisher_Id]=1
go
update V_Ilkiv_Library.dbo.Books  
set [title] = 'incredible title',
	[edition] = ABS(CHECKSUM(NEWID()) % 10000) + 70, 
	[published] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 10000), '1990-01-01'), 
	[issue] = ABS(CHECKSUM(NEWID()) % 5) + 1
	where V_Ilkiv_Library.dbo.Books.[Publisher_Id]=2
go
update V_Ilkiv_Library.dbo.Books  
set [title] = 'incredible title',
	[edition] = ABS(CHECKSUM(NEWID()) % 10000) + 70, 
	[published] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 10000), '1990-01-01'), 
	[issue] = ABS(CHECKSUM(NEWID()) % 5) + 1
	where V_Ilkiv_Library.dbo.Books.[Publisher_Id]=3
go
update V_Ilkiv_Library.dbo.Books  
set [title] = 'incredible title',
	[edition] = ABS(CHECKSUM(NEWID()) % 10000) + 70, 
	[published] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 10000), '1990-01-01'), 
	[issue] = ABS(CHECKSUM(NEWID()) % 5) + 1
	where V_Ilkiv_Library.dbo.Books.[Publisher_Id]=4
go
update V_Ilkiv_Library.dbo.Books  
set [title] = 'incredible title',
	[edition] = ABS(CHECKSUM(NEWID()) % 10000) + 70, 
	[published] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 10000), '1990-01-01'), 
	[issue] = ABS(CHECKSUM(NEWID()) % 5) + 1
	where V_Ilkiv_Library.dbo.Books.[Publisher_Id]=5
go
update V_Ilkiv_Library.dbo.Books  
set [title] = 'incredible title',
	[edition] = ABS(CHECKSUM(NEWID()) % 10000) + 70, 
	[published] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 10000), '1990-01-01'), 
	[issue] = ABS(CHECKSUM(NEWID()) % 5) + 1
	where V_Ilkiv_Library.dbo.Books.[Publisher_Id]=6
go
update V_Ilkiv_Library.dbo.Books  
set [title] = 'incredible title',
	[edition] = ABS(CHECKSUM(NEWID()) % 10000) + 70, 
	[published] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 10000), '1990-01-01'), 
	[issue] = ABS(CHECKSUM(NEWID()) % 5) + 1
	where V_Ilkiv_Library.dbo.Books.[Publisher_Id]=7
go
update V_Ilkiv_Library.dbo.Books  
set [title] = 'incredible title',
	[edition] = ABS(CHECKSUM(NEWID()) % 10000) + 70, 
	[published] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 10000), '1990-01-01'), 
	[issue] = ABS(CHECKSUM(NEWID()) % 5) + 1
	where V_Ilkiv_Library.dbo.Books.[Publisher_Id]=8
go
update V_Ilkiv_Library.dbo.Books  
set [title] = 'incredible title',
	[edition] = ABS(CHECKSUM(NEWID()) % 10000) + 70, 
	[published] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 10000), '1990-01-01'), 
	[issue] = ABS(CHECKSUM(NEWID()) % 5) + 1
	where V_Ilkiv_Library.dbo.Books.[Publisher_Id]=9
go
update V_Ilkiv_Library.dbo.Books  
set [title] = 'incredible title',
	[edition] = ABS(CHECKSUM(NEWID()) % 10000) + 70, 
	[published] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 10000), '1990-01-01'), 
	[issue] = ABS(CHECKSUM(NEWID()) % 5) + 1
	where V_Ilkiv_Library.dbo.Books.[Publisher_Id]=10
go
update V_Ilkiv_Library.dbo.Books  
set [title] = 'incredible title',
	[edition] = ABS(CHECKSUM(NEWID()) % 10000) + 70, 
	[published] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 10000), '1990-01-01'), 
	[issue] = ABS(CHECKSUM(NEWID()) % 5) + 1
	where V_Ilkiv_Library.dbo.Books.[Publisher_Id]=11
go
update V_Ilkiv_Library.dbo.Books  
set [title] = 'incredible title',
	[edition] = ABS(CHECKSUM(NEWID()) % 10000) + 70, 
	[published] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 10000), '1990-01-01'), 
	[issue] = ABS(CHECKSUM(NEWID()) % 5) + 1
	where V_Ilkiv_Library.dbo.Books.[Publisher_Id]=12
go
update V_Ilkiv_Library.dbo.Books  
set [title] = 'incredible title',
	[edition] = ABS(CHECKSUM(NEWID()) % 10000) + 70, 
	[published] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 10000), '1990-01-01'), 
	[issue] = ABS(CHECKSUM(NEWID()) % 5) + 1
	where V_Ilkiv_Library.dbo.Books.[Publisher_Id]=13
go
update V_Ilkiv_Library.dbo.Books  
set [title] = 'incredible title',
	[edition] = ABS(CHECKSUM(NEWID()) % 10000) + 70, 
	[published] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 10000), '1990-01-01'), 
	[issue] = ABS(CHECKSUM(NEWID()) % 5) + 1
	where V_Ilkiv_Library.dbo.Books.[Publisher_Id]=14
go


---------------------------Update Publishers
UPDATE V_Ilkiv_Library.dbo.Publishers 
set [created] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[country] = DEFAULT,
	[City] = DEFAULT,
	[book_amount] = DEFAULT,
	[issue_amount] = DEFAULT,
	[total_edition] = DEFAULT
	where [V_Ilkiv_Library].[dbo].[Publishers].[Publisher_Id]=1
go

UPDATE V_Ilkiv_Library.dbo.Publishers 
set [created] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[country] = DEFAULT,
	[City] = DEFAULT,
	[book_amount] = DEFAULT,
	[issue_amount] = DEFAULT,
	[total_edition] = DEFAULT
	where [V_Ilkiv_Library].[dbo].[Publishers].[Publisher_Id]=2
go

UPDATE V_Ilkiv_Library.dbo.Publishers 
set [created] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[country] = DEFAULT,
	[City] = DEFAULT,
	[book_amount] = DEFAULT,
	[issue_amount] = DEFAULT,
	[total_edition] = DEFAULT
	where [V_Ilkiv_Library].[dbo].[Publishers].[Publisher_Id]=3
go

UPDATE V_Ilkiv_Library.dbo.Publishers 
set [created] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[country] = DEFAULT,
	[City] = DEFAULT,
	[book_amount] = DEFAULT,
	[issue_amount] = DEFAULT,
	[total_edition] = DEFAULT
	where [V_Ilkiv_Library].[dbo].[Publishers].[Publisher_Id]=4
go

UPDATE V_Ilkiv_Library.dbo.Publishers 
set [created] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[country] = DEFAULT,
	[City] = DEFAULT,
	[book_amount] = DEFAULT,
	[issue_amount] = DEFAULT,
	[total_edition] = DEFAULT
	where [V_Ilkiv_Library].[dbo].[Publishers].[Publisher_Id]=5
go

UPDATE V_Ilkiv_Library.dbo.Publishers 
set [created] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[country] = DEFAULT,
	[City] = DEFAULT,
	[book_amount] = DEFAULT,
	[issue_amount] = DEFAULT,
	[total_edition] = DEFAULT
	where [V_Ilkiv_Library].[dbo].[Publishers].[Publisher_Id]=6
go

UPDATE V_Ilkiv_Library.dbo.Publishers 
set [created] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[country] = DEFAULT,
	[City] = DEFAULT,
	[book_amount] = DEFAULT,
	[issue_amount] = DEFAULT,
	[total_edition] = DEFAULT
	where [V_Ilkiv_Library].[dbo].[Publishers].[Publisher_Id]=7
go

UPDATE V_Ilkiv_Library.dbo.Publishers 
set [created] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[country] = DEFAULT,
	[City] = DEFAULT,
	[book_amount] = DEFAULT,
	[issue_amount] = DEFAULT,
	[total_edition] = DEFAULT
	where [V_Ilkiv_Library].[dbo].[Publishers].[Publisher_Id]=8
go

UPDATE V_Ilkiv_Library.dbo.Publishers 
set [created] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[country] = DEFAULT,
	[City] = DEFAULT,
	[book_amount] = DEFAULT,
	[issue_amount] = DEFAULT,
	[total_edition] = DEFAULT
	where [V_Ilkiv_Library].[dbo].[Publishers].[Publisher_Id]=9
go

UPDATE V_Ilkiv_Library.dbo.Publishers 
set [created] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[country] = DEFAULT,
	[City] = DEFAULT,
	[book_amount] = DEFAULT,
	[issue_amount] = DEFAULT,
	[total_edition] = DEFAULT
	where [V_Ilkiv_Library].[dbo].[Publishers].[Publisher_Id]=10
go

UPDATE V_Ilkiv_Library.dbo.Publishers 
set [created] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[country] = DEFAULT,
	[City] = DEFAULT,
	[book_amount] = DEFAULT,
	[issue_amount] = DEFAULT,
	[total_edition] = DEFAULT
	where [V_Ilkiv_Library].[dbo].[Publishers].[Publisher_Id]=11
go

UPDATE V_Ilkiv_Library.dbo.Publishers 
set [created] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[country] = DEFAULT,
	[City] = DEFAULT,
	[book_amount] = DEFAULT,
	[issue_amount] = DEFAULT,
	[total_edition] = DEFAULT
	where [V_Ilkiv_Library].[dbo].[Publishers].[Publisher_Id]=12
go

UPDATE V_Ilkiv_Library.dbo.Publishers 
set [created] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[country] = DEFAULT,
	[City] = DEFAULT,
	[book_amount] = DEFAULT,
	[issue_amount] = DEFAULT,
	[total_edition] = DEFAULT
	where [V_Ilkiv_Library].[dbo].[Publishers].[Publisher_Id]=13
go

UPDATE V_Ilkiv_Library.dbo.Publishers 
set [created] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[country] = DEFAULT,
	[City] = DEFAULT,
	[book_amount] = DEFAULT,
	[issue_amount] = DEFAULT,
	[total_edition] = DEFAULT
	where [V_Ilkiv_Library].[dbo].[Publishers].[Publisher_Id]=14
go

