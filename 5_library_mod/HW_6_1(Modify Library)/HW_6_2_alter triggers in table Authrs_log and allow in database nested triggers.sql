﻿use V_Ilkiv_Library
go

drop trigger if exists [dbo].[tr_Authors_log_I] 
go

drop trigger if exists [dbo].[tr_Authors_log_D] 
go

drop trigger if exists [dbo].[tr_Authors_log_U] 
go
-- INSERT TRIGGER

create or alter trigger [dbo].[tr_Authors_log_I] on [dbo].[Authors]
after insert
as
begin
declare @operation varchar(7) = 'INSERT'
declare @update_date datetime = GETDATE();
insert into [dbo].[Authors_log](
[Author_Id_new], [Name_new], [URL_new], [Author_Id_old], 
[Name_old], [URL_old], [operation type], [operation_datetime],
[book_amount_old], [issue_amount_old], [total_edition_old],
[book_amount_new]  , [issue_amount_new]  , [total_edition_new]   
)
select [Author_Id], [Name], [URL], null, null,
null, @operation, @update_date, null, null, null, [book_amount], [issue_amount], [total_edition] from INSERTED
end 
go

-- DELETE TRIGGER

create or alter trigger [dbo].[tr_Authors_log_D] on [dbo].[Authors]
after delete
as
begin
declare @operation varchar(7) = 'DELETE'
declare @update_date datetime = GETDATE();
insert into [dbo].[Authors_log](
[Author_Id_new], [Name_new], [URL_new], [Author_Id_old], 
[Name_old], [URL_old], [operation type], [operation_datetime],
[book_amount_old], [issue_amount_old], [total_edition_old],
[book_amount_new]  , [issue_amount_new]  , [total_edition_new] 
)
select null, null, null, [Author_Id], [Name],
[URL], @operation, @update_date, [book_amount], [issue_amount], [total_edition], null, null, null from DELETED
end 
go

-- UPDATE TRIGGER

create or alter trigger [dbo].[tr_Authors_log_U] on [dbo].[Authors]
after update
as
begin
declare @operation varchar(7) = 'UPDATE'
declare @update_date datetime = GETDATE();
insert into [dbo].[Authors_log](
[Author_Id_new], [Name_new], [URL_new], [Author_Id_old], 
[Name_old], [URL_old], [operation type], [operation_datetime],
[book_amount_old], [issue_amount_old], [total_edition_old],
[book_amount_new]  , [issue_amount_new]  , [total_edition_new]  
)
select inserted.[Author_Id], inserted.[Name], inserted.[URL], DELETED.[Author_Id], DELETED.[Name],
DELETED.[URL], @operation, @update_date, DELETED.[book_amount], DELETED.[issue_amount], DELETED.[total_edition], 
inserted.[book_amount], inserted.[issue_amount], inserted.[total_edition]  from DELETED, inserted
end 
go

exec sp_configure 'show advanced options', 1;  
go  
reconfigure ;  
go  
exec sp_configure 'nested triggers', 0 ;  
go  
reconfigure;  
go