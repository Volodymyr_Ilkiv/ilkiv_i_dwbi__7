﻿use triger_fk_cursor
go

declare @Surname varchar(30), @Name varchar(30)
declare @Table1 nvarchar(max), @Table2 nvarchar(max)
declare @ColumnNum int;
declare cr_crea_table cursor
	for select [surname], [name] FROM [employee]

OPEN cr_crea_table
	fetch next from cr_crea_table INTO @Surname, @Name
	while @@FETCH_STATUS = 0
	begin
		set @Table2 = '';
		set @ColumnNum = FLOOR(RAND()*(9-1+1)+1); 
		set @Table1 = 'CREATE TABLE ' + @Surname + @Name + ' ('
		while @ColumnNum > 0
		begin
			set @Table2 = @Table2 + ' column' + CONVERT(char(1), @ColumnNum) + ' INT, ';
			set @ColumnNum = @ColumnNum - 1;
		end
		set @Table2 = LEFT(@Table2, LEN(@Table2) - 1) 
		set @Table2 = @Table2 + ')'
		exec(@Table1 + @Table2)
		fetch next from cr_crea_table into @Surname, @Name
	end
close cr_crea_table
deallocate cr_crea_table
go