﻿use triger_fk_cursor
go

declare @id				 int,
		@surname		 varchar(30),
		@name			 varchar(30),
		@midle_name		 varchar(30),
		@identity_number char(10),
		@passport		 char(10),
		@experience		 decimal(10, 1),
		@birthday		 date,
		@post			 varchar(15),
		@pharmacy_id	 int
declare @SQLquery1 nvarchar(max)
declare @RandomBIT bit
declare @Num int, @RowsCount int
declare @Time varchar(10)
declare cr_isert_table cursor
	for select id, surname, name, midle_name, identity_number, 
				passport, experience, birthday, post, pharmacy_id 
	from [employee]

open cr_isert_table

	set @Time = (select CONVERT(varchar(10), FORMAT(GETDATE() , 'HH_mm_ss')))
	set @Num = 2
	set @RowsCount = (select COUNT(id) from [employee])

	while @Num > 0
	begin
		set @SQLquery1 = 
		'CREATE TABLE [' + CONVERT(char(1), @Num) + '_employee_' + @Time + '] (
			id                 int,
			surname            varchar(30),
			name               char(30),
			midle_name         varchar(30),
			identity_number    char(10),
			passport           char(10),
			experience         decimal(10, 1),
			birthday           date,
			post               varchar(15),
			pharmacy_id        int)'
		exec(@SQLquery1)
		set @Num = @Num - 1
	end

	fetch next from cr_isert_table 
	into @id, @Surname, @Name, @midle_name, @identity_number, 
		 @passport, @experience, @birthday, @post, @pharmacy_id

	while @@FETCH_STATUS = 0
	begin

		set @RandomBIT = CAST(ROUND(RAND(),0) AS BIT) -- random 1 or 0
		if @RandomBIT = 1
			exec('insert into [1_employee_' + @Time + '] select * from employee where id = ' + @id)
		else
			exec('insert into [2_employee_' + @Time + '] select * from employee where id = ' + @id)

		fetch next from cr_isert_table 
		into @id, @Surname, @Name, @midle_name, @identity_number, 
				@passport, @experience, @birthday, @post, @pharmacy_id

	end
close cr_isert_table
deallocate cr_isert_table
go
