﻿use triger_fk_cursor
go

drop trigger if exists tr_employyee_pharmacy
go
drop trigger if exists tr_pharmacy_medicine
go
drop trigger if exists tr_medicine_zone
go
drop trigger if exists tr_pharmacy_street
go
drop trigger if exists tr_post
go
drop trigger if exists tr_identity_number
go
drop trigger if exists tr_ministry_code
go

-----------------------------------create trigger tr_employyee_pharmacy

create trigger tr_employyee_pharmacy ON employee
after insert, update
as
if ((select pharmacy_id from inserted)  not in (select id from pharmacy))
	rollback transaction
if ((select post from inserted) not in (select post from post))
	rollback transaction
	Begin
	print N'inserting or updating records is limited by foreign key key'
	end
go

-----------------------------------create trigger tr_pharmacy_medicine

create trigger tr_pharmacy_medicine ON pharmacy_medicine
after insert, update
as
if ((select pharmacy_id from inserted)  not in (select id from pharmacy)) OR 
   ((select medicine_id from inserted)  not in (select id from medicine))
	rollback transaction
	Begin
	print N'inserting or updating records is limited by foreign key key'
	end
go

-----------------------------------create trigger tr_medicine_zone

create trigger tr_medicine_zone ON medicine_zone
after insert, update
as
if ((select medicine_id from inserted)  not in (select id from medicine)) OR 
   ((select zone_id from inserted)  not in (select id from zone))
	rollback transaction
	Begin
	print N'inserting or updating records is limited by foreign key key'
	end
go

-----------------------------------create trigger tr_pharmacy_street

create trigger tr_pharmacy_street ON pharmacy
after insert, update
as
if ((select street from inserted)  not in (select street from street))
	rollback transaction
	Begin
	print N'inserting or updating records is limited by foreign key key'
	end
go

-----------------------------------create trigger tr_post

create trigger tr_post ON post
instead of delete, insert, update 
as
print N'You can not change the row in this table'
rollback transaction
return
go

-----------------------------------create trigger tr_identity_number

create trigger tr_identity_number ON employee
after insert, update 
as
if ((select identity_number from inserted) LIKE '%00')
print N'identity_number can not end on 00'
rollback transaction
return
go

-----------------------------------create trigger tr_ministry_code

create trigger tr_ministry_code ON medicine
after insert, update 
as
if ((SELECT ministry_code FROM inserted)NOT LIKE '[a-z]%')
	or
 ((SELECT ministry_code FROM inserted)NOT LIKE '_[a-z]%')
	or
 ((SELECT ministry_code FROM inserted) LIKE '[mp]%')
or
 ((SELECT ministry_code FROM inserted) LIKE '_[mp]%')
	or
 ((SELECT ministry_code FROM inserted) NOT LIKE '__[-]%')
	or
 ((SELECT ministry_code FROM inserted) NOT LIKE '___[0-9]%')
	or
 ((SELECT ministry_code FROM inserted) NOT LIKE '____[0-9]%')
	or
 ((SELECT ministry_code FROM inserted) NOT LIKE '_____[0-9]%')
	or
	 ((SELECT ministry_code FROM inserted) NOT LIKE '______[-]%')
	or
	 ((SELECT ministry_code FROM inserted) NOT LIKE '_______[0-9]%')
	or
	 ((SELECT ministry_code FROM inserted) NOT LIKE '________[0-9]')

rollback transaction
Begin
	print N'format ministry_code must be ([A-Z except M and P]-[three number]-[two number])'
	end
go
