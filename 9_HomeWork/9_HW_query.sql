﻿use labor_sql
go

---1
;with laptop_cte as
(
select * from laptop
)
select  top 5 * from laptop_cte
go

---2
;with 
laptop_cte as
(
select * from laptop
),
laptop2_cte as
(
select * from laptop_cte where hd=10
)
select top 5 * from laptop2_cte
go

---3
--select [region_id], [place_id], [name], [placelevel] from(select )

---create table #geografy_level1([region_id] int,[place_id] int,[name] varchar(20),[placelevel] int)insert into [region_id], [place_id], [name], [placelevel] values()go
;with geography_cte as
(
select [region_id], [id] as [place_id], [name],0 as [placelevel] from dbo.[geography] where region_id is null
	union all
	select geo.[region_id], geo.[id] , geo.[name], g_cte.[placelevel]+1 from [geography] as geo
		inner join geography_cte as g_cte on geo.[region_id]=g_cte.[place_id]
)
select * from geography_cte
where placelevel=1
go

---4
declare @region as varchar(25)
set @region='Ivano-Frankivsk'
;with geography_cte as
(
select [region_id], [id] as [place_id], [name],-1 as [placelevel] from dbo.[geography] where name=@region
	union all
	select geo.[region_id], geo.[id] , geo.[name], g_cte.[placelevel]+1 from [geography] as geo
		inner join geography_cte as g_cte on geo.[region_id]=g_cte.[place_id]
)
select * from geography_cte
where placelevel>=0
go

---5
;with recursion_cte(x)
as
(
  select 1 as x
    union all
  select x+1
  from recursion_cte
  where x<10000
)
select x from recursion_cte
option (maxrecursion 10000);

---6
;with recursion_cte(x)
as
(
  select 1 as x
    union all
  select x+1
  from recursion_cte
  where x<100000
)
select x from recursion_cte
option (maxrecursion 0);

---7
;WITH dates_cte([date])
AS
(
  SELECT CAST(CONCAT(YEAR(GETDATE()),'0101') AS datetime) AS [date]
  
  UNION ALL
  
  SELECT DATEADD(d,1,[date])
  FROM dates_cte
  WHERE [date] < CONCAT(YEAR(GETDATE()),'1231')
)
SELECT COUNT([date]) AS n_sat_sun
FROM dates_cte 
WHERE DATENAME(dw,[date]) = 'Sunday' OR DATENAME(dw,[date]) = 'Saturday'
OPTION (MAXRECURSION 0);

---8
select distinct [maker]
from product where product.type='PC' and
	product.maker not in(select maker from product where product.type='Laptop')
;

---9
select distinct [maker]
from product where product.type='PC' and
	product.maker != all (select maker from product where product.type='Laptop')
;

---10
select distinct [maker]
from product where product.type='PC' and not
	product.maker = any (select maker from product where product.type='Laptop')
;

---11
select distinct [maker]
from product where product.type='PC' and
	product.maker in (select maker from product where product.type='Laptop')
	;

---12
select distinct [maker]
from product where product.type='PC' and not
	product.maker != all (select maker from product where product.type='Laptop')
;

---13
select distinct [maker]
from product where product.type='PC' and 
	product.maker = any (select maker from product where product.type='Laptop')
;

---14
select distinct [maker]
from product p1 where p1.type='PC' and
 not exists (select 1 from product p2
			left join pc on p2.model=pc.model 
					where p2.maker=p1.maker and p2.type='PC' and pc.model is null)
;

---15
;WITH country_cte
AS
(
	SELECT country, class,
	       CASE country
		     WHEN 'Ukraine' THEN 1
			 ELSE 0
		   END AS ukr_id
	FROM classes
)
SELECT country, class
FROM country_cte
WHERE ukr_id=CASE
               WHEN (SELECT SUM(ukr_id) FROM country_cte)>0 THEN 1
			   ELSE 0
			 END;

---16
SELECT o.ship, o.battle, (SELECT [date] FROM battles WHERE [name]=o.battle) AS [date]
FROM outcomes AS o
WHERE o.result='damaged'
      AND EXISTS (SELECT 1
	              FROM outcomes AS o2
				  WHERE o2.ship=o.ship AND 
				  (SELECT [date] FROM battles WHERE [name]=o2.battle)>(SELECT [date] FROM battles WHERE [name]=o.battle) 
				  );

---17
SELECT DISTINCT pr1.maker
FROM product AS pr1
WHERE [type]='PC'
      AND NOT EXISTS (SELECT 1
	                  FROM product AS pr2
					    LEFT JOIN pc
						  ON pr2.model=pc.model
					  WHERE pr2.maker=pr1.maker AND pr2.[type]='PC' AND pc.model IS NULL)
					  ;

---18
SELECT DISTINCT maker
FROM product
WHERE [type]='Printer' 
      AND maker IN (SELECT maker
	                FROM product
					WHERE model IN (SELECT model FROM PC
									WHERE speed=(SELECT MAX(speed) FROM PC)
									)
					)
;

---19
SELECT s.class
FROM outcomes AS o
  INNER JOIN ships AS s
    ON o.ship=s.[name]
WHERE o.result='sunk'

UNION

	SELECT c.class
	FROM outcomes AS o
            INNER JOIN classes AS c
		ON o.ship=c.class
      WHERE o.result='sunk'
	  ;

---20
select model, price
from printer
where price=(select MAX(price) from printer)
;

---21
select 'Laptop' as [type], model, speed 
from laptop
where speed<all(select speed from pc)
;

---22
SELECT DISTINCT maker, (SELECT MIN(price) FROM printer WHERE color='y')  AS price
FROM product
WHERE model IN(
			SELECT model
			FROM printer
			WHERE color='y' 
				  AND price=(SELECT MIN(price)
							 FROM printer
							 WHERE color='y'));

---23
SELECT o.battle, c.country, COUNT(o.ship) AS ship_number
FROM outcomes AS o
  INNER JOIN ships AS s
    ON o.ship=s.[name]
	 INNER JOIN classes AS c
	   ON s.class=c.class
GROUP BY c.country, o.battle
HAVING COUNT(o.ship)>1
;

---24
SELECT DISTINCT maker, 
       (SELECT COUNT(code) FROM pc WHERE model IN (SELECT model FROM product WHERE maker=p.maker)) AS pc,
	   (SELECT COUNT(code) FROM laptop WHERE model IN (SELECT model FROM product WHERE maker=p.maker)) AS laptop,
	   (SELECT COUNT(code) FROM printer WHERE model IN (SELECT model FROM product WHERE maker=p.maker)) AS printer
FROM product AS p
;

---25
;WITH C
AS
(
SELECT DISTINCT maker, 
       (SELECT COUNT(code) FROM pc WHERE model IN (SELECT model FROM product WHERE maker=p.maker)) AS pc
FROM product AS p)
SELECT maker,
       CASE
	      WHEN pc=0 THEN 'no'
		  ELSE 'yes(' +CAST(pc AS VARCHAR(10)) + ')'
	   END AS pc  
FROM C
;

---26
;WITH c
AS
(
	SELECT point, [date]
	FROM income_o
		UNION
			SELECT point, [date]
	FROM outcome_o
)
SELECT c.point, c.[date], COALESCE([io].inc,0) AS inc, COALESCE(oo.[out],0) AS [out]
FROM c
  LEFT JOIN income_o AS [io]
    ON c.point=[io].point AND c.[date]=[io].[date]
	 LEFT JOIN outcome_o AS oo
	   ON c.point=oo.point AND c.[date]=oo.[date]
;

---27
SELECT s.[name], c.numGuns, c.bore, c.displacement,
       c.[type], c.country, s.launched, c.class
FROM ships AS s
  INNER JOIN classes AS c
    ON s.class=c.class 
WHERE 
     ((CASE c.numGuns WHEN 8 THEN 1 ELSE 0 END) +
	 (CASE c.bore WHEN 15 THEN 1 ELSE 0 END) +
	 (CASE c.displacement WHEN 32000 THEN 1 ELSE 0 END) +
	 (CASE c.[type] WHEN 'bb' THEN 1 ELSE 0 END) +
	 (CASE c.country WHEN 'USA' THEN 1 ELSE 0 END) +
	 (CASE s.launched WHEN 1915 THEN 1 ELSE 0 END) +
	 (CASE c.class WHEN 'Kon' THEN 1 ELSE 0 END))>3
;

---28
;WITH point_date_cte
AS
(
  SELECT point, [date]
  FROM outcome_o
    UNION
	  SELECT point, [date]
  FROM outcome
),
 outcome_cte
 AS
 (
   SELECT point, [date], SUM([out]) AS [out]
   FROM outcome
   GROUP BY point, [date]
 ),
	 aggr_out_cte
 AS
 (
   SELECT pdc.point, pdc.[date], COALESCE(oo.[out],0) AS once_day_out, COALESCE(oc.[out],0) AS more_once_out
   FROM point_date_cte pdc
     LEFT JOIN outcome_o AS oo
	   ON pdc.point=oo.point and pdc.[date]=oo.[date]
	     LEFT JOIN outcome_cte AS oc
		   ON pdc.point=oc.point and pdc.[date]=oc.[date]
 )
  SELECT point, [date],
        CASE
		 WHEN once_day_out>more_once_out THEN 'once a day'
		 WHEN once_day_out<more_once_out THEN 'more than once a day'
		 ELSE 'both'
		END AS lider
 FROM aggr_out_cte
 ;

---29
SELECT pr.maker, pr.model, pr.[type], pc.price
FROM product AS pr
  INNER JOIN pc
    ON pr.model=pc.model AND pr.maker='B'
	UNION ALL
	SELECT pr.maker, pr.model, pr.[type], prn.price
FROM product AS pr
  INNER JOIN printer AS prn
    ON pr.model=prn.model AND pr.maker='B'
	UNION ALL
	SELECT pr.maker, pr.model, pr.[type], l.price
FROM product AS pr
  INNER JOIN laptop AS l
    ON pr.model=l.model AND pr.maker='B'
;

---30
SELECT [name], class
FROM ships
WHERE class=[name]
	UNION
	SELECT o.ship, c.class
	FROM outcomes AS o
  INNER JOIN classes AS c
    ON o.ship=c.class
;

---31
USE labor_sql;
;WITH ship_cte
AS
(
	SELECT class, [name]
	FROM ships
		UNION
			SELECT c.class, o.ship
	FROM outcomes AS o
	  INNER JOIN classes AS c
		ON o.ship=c.class
)
SELECT class
FROM ship_cte
GROUP BY class
HAVING COUNT([name])=1
;

---32
SELECT [name]
FROM ships
WHERE launched<1942
	UNION
	SELECT o.ship
	FROM outcomes AS o
  INNER JOIN battles AS b
    ON o.battle=b.[name]
WHERE b.[date]<'19420101'
;