﻿use V_Ilkiv_Library
go

update  V_Ilkiv_Library.dbo.Authors
set V_Ilkiv_Library.dbo.Authors.[Name]+=' good author'
where [Author_Id] =1
go
update  V_Ilkiv_Library.dbo.Authors
set V_Ilkiv_Library.dbo.Authors.[Name]+=' good author'
where [Author_Id] =2
go
update  V_Ilkiv_Library.dbo.Authors
set V_Ilkiv_Library.dbo.Authors.[Name]+=' good author'
where [Author_Id] =3
go
update  V_Ilkiv_Library.dbo.Authors
set V_Ilkiv_Library.dbo.Authors.[Name]+=' good author'
where [Author_Id] =4
go
update  V_Ilkiv_Library.dbo.Authors
set V_Ilkiv_Library.dbo.Authors.[Name]+=' good author'
where [Author_Id] =5
go
update  V_Ilkiv_Library.dbo.Authors
set V_Ilkiv_Library.dbo.Authors.[Name]+=' good author'
where [Author_Id] =6
go
update  V_Ilkiv_Library.dbo.Authors
set V_Ilkiv_Library.dbo.Authors.[Name]+=' good author'
where [Author_Id] =7
go
update  V_Ilkiv_Library.dbo.Authors
set V_Ilkiv_Library.dbo.Authors.[Name]+=' good author'
where [Author_Id] =8
go
update  V_Ilkiv_Library.dbo.Authors
set V_Ilkiv_Library.dbo.Authors.[Name]+=' good author'
where [Author_Id] =9
go
update  V_Ilkiv_Library.dbo.Authors
set V_Ilkiv_Library.dbo.Authors.[Name]+=' good author'
where [Author_Id] =10
go
----------------------------------------------

update V_Ilkiv_Library.dbo.Books
set [URL]=[URL]+'.net'
where [Publisher_Id]=1
go
update V_Ilkiv_Library.dbo.Books
set [URL]=[URL]+'.net'
where [Publisher_Id]=2
go
update V_Ilkiv_Library.dbo.Books
set [URL]=[URL]+'.net'
where [Publisher_Id]=3
go
update V_Ilkiv_Library.dbo.Books
set [URL]=[URL]+'.net'
where [Publisher_Id]=4
go
update V_Ilkiv_Library.dbo.Books
set [URL]=[URL]+'.net'
where [Publisher_Id]=5
go
update V_Ilkiv_Library.dbo.Books
set [URL]=[URL]+'.net'
where [Publisher_Id]=6
go
update V_Ilkiv_Library.dbo.Books
set [URL]=[URL]+'.net'
where [Publisher_Id]=7
go
update V_Ilkiv_Library.dbo.Books
set [URL]=[URL]+'.net'
where [Publisher_Id]=8
go
update V_Ilkiv_Library.dbo.Books
set [URL]=[URL]+'.net'
where [Publisher_Id]=9
go
update V_Ilkiv_Library.dbo.Books
set [URL]=[URL]+'.net'
where [Publisher_Id]=10
go
----------------------------------------------------------

update V_Ilkiv_Library.dbo.BooksAuthors
set [Seq_No]=[Seq_No]+1
where [BooksAuthors_id]=1
go
update V_Ilkiv_Library.dbo.BooksAuthors
set [Seq_No]=[Seq_No]+1
where [BooksAuthors_id]=2
go
update V_Ilkiv_Library.dbo.BooksAuthors
set [Seq_No]=[Seq_No]+1
where [BooksAuthors_id]=3
go
update V_Ilkiv_Library.dbo.BooksAuthors
set [Seq_No]=[Seq_No]+1
where [BooksAuthors_id]=4
go
update V_Ilkiv_Library.dbo.BooksAuthors
set [Seq_No]=[Seq_No]+1
where [BooksAuthors_id]=5
go
update V_Ilkiv_Library.dbo.BooksAuthors
set [Seq_No]=[Seq_No]+1
where [BooksAuthors_id]=6
go
update V_Ilkiv_Library.dbo.BooksAuthors
set [Seq_No]=[Seq_No]+1
where [BooksAuthors_id]=7
go
update V_Ilkiv_Library.dbo.BooksAuthors
set [Seq_No]=[Seq_No]+1
where [BooksAuthors_id]=8
go
update V_Ilkiv_Library.dbo.BooksAuthors
set [Seq_No]=[Seq_No]+1
where [BooksAuthors_id]=9
go
update V_Ilkiv_Library.dbo.BooksAuthors
set [Seq_No]=[Seq_No]+1
where [BooksAuthors_id]=10
go
-----------------------------------------------

update V_Ilkiv_Library.dbo.Publishers
set [URL]=[URL]+'.net'
where [Publisher_Id]=1
go
update V_Ilkiv_Library.dbo.Publishers
set [URL]=[URL]+'.net'
where [Publisher_Id]=2
go
update V_Ilkiv_Library.dbo.Publishers
set [URL]=[URL]+'.net'
where [Publisher_Id]=3
go
update V_Ilkiv_Library.dbo.Publishers
set [URL]=[URL]+'.net'
where [Publisher_Id]=4
go
update V_Ilkiv_Library.dbo.Publishers
set [URL]=[URL]+'.net'
where [Publisher_Id]=5
go
update V_Ilkiv_Library.dbo.Publishers
set [URL]=[URL]+'.net'
where [Publisher_Id]=6
go
update V_Ilkiv_Library.dbo.Publishers
set [URL]=[URL]+'.net'
where [Publisher_Id]=7
go
update V_Ilkiv_Library.dbo.Publishers
set [URL]=[URL]+'.net'
where [Publisher_Id]=8
go
update V_Ilkiv_Library.dbo.Publishers
set [URL]=[URL]+'.net'
where [Publisher_Id]=9
go
update V_Ilkiv_Library.dbo.Publishers
set [URL]=[URL]+'.net'
where [Publisher_Id]=10
go