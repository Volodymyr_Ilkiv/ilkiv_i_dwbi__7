﻿use V_Ilkiv_Library_view
go

drop view if exists [Authors_view]
go
drop view if exists [Authors_log_view]
go
drop view if exists [Books_view]
go
drop view if exists [BooksAuthors_view]
go
drop view if exists [Publishers_view]
go

create or alter view [Authors_view] as
select * from V_Ilkiv_Library.dbo.Authors
go

create or alter view [Authors_log_view] as
select * from V_Ilkiv_Library.dbo.Authors_log
go

create or alter view [Books_view] as
select * from V_Ilkiv_Library.dbo.Books
go

create or alter view [BooksAuthors_view] as
select * from V_Ilkiv_Library.dbo.BooksAuthors
go

create or alter view [Publishers_view] as
select * from V_Ilkiv_Library.dbo.Publishers
go