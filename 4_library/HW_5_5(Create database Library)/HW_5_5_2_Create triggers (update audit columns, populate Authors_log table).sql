﻿use V_Ilkiv_Library
go

drop trigger if exists [dbo].[tr_Authors_log_I] 
go

drop trigger if exists [dbo].[tr_Authors_log_D] 
go

drop trigger if exists [dbo].[tr_Authors_log_U] 
go
-- INSERT TRIGGER

create or alter trigger [dbo].[tr_Authors_log_I] on [dbo].[Authors]
after insert
as
begin
declare @operation varchar(7) = 'INSERT'
declare @update_date datetime = GETDATE();
insert into [dbo].[Authors_log](
[Author_Id_new], [Name_new], [URL_new], [Author_Id_old], 
[Name_old], [URL_old], [operation type], [operation_datetime] 
)
select [Author_Id], [Name], [URL], null, null,
null, @operation, @update_date from INSERTED
end 
go

-- DELETE TRIGGER

create or alter trigger [dbo].[tr_Authors_log_D] on [dbo].[Authors]
after delete
as
begin
declare @operation varchar(7) = 'DELETE'
declare @update_date datetime = GETDATE();
insert into [dbo].[Authors_log](
[Author_Id_new], [Name_new], [URL_new], [Author_Id_old], 
[Name_old], [URL_old], [operation type], [operation_datetime] 
)
select null, null, null, [Author_Id], [Name],
[URL], @operation, @update_date from DELETED
end 
go

-- UPDATE TRIGGER

create or alter trigger [dbo].[tr_Authors_log_U] on [dbo].[Authors]
after update
as
begin
declare @operation varchar(7) = 'UPDATE'
declare @update_date datetime = GETDATE();
insert into [dbo].[Authors_log](
[Author_Id_new], [Name_new], [URL_new], [Author_Id_old], 
[Name_old], [URL_old], [operation type], [operation_datetime] 
)
select inserted.[Author_Id], inserted.[Name], inserted.[URL], DELETED.[Author_Id], DELETED.[Name],
DELETED.[URL], @operation, @update_date from DELETED, inserted
end 
go
