﻿use V_Ilkiv_Library
go
--------------drop if exists
drop table if exists [dbo].[Authors]
go
drop table if exists [dbo].[Books]
go
drop table if exists [dbo].[BooksAuthors]
go
drop table if exists [dbo].[Publishers]
go
drop table if exists [dbo].[Authors_log]
go


----create tables

create table [dbo].[Authors]
(
[Author_Id]   int not null IDENTITY 
              constraint pk_Author_id primary key (Author_id),
[Name]        varchar(250) not null
              constraint uniq_Name unique ([Name]),
[URL]         varchar(250) not null,
[inserted]    datetime not null,
[inserted_by] varchar(100) not null,
[updated]     datetime null,
[updated_by]  varchar(100) null

)
go

create table [dbo].[Books]
(
[ISBN]            varchar(100) not null  
                  constraint pk_ISBN primary key (ISBN),
[Publisher_Id]    int not null,
[URL]             varchar(250) not null
                  constraint uniq_URL unique ([URL]),
[Price]           decimal(20,4) not null
                  constraint chk_Price check ([Price]>=0),
[inserted]        datetime not null,
[inserted_by]     varchar(100) not null,
[updated]         datetime null,
[updated_by]      varchar(100)  null
)
go

create table [dbo].[BooksAuthors]
(
[BooksAuthors_id] int not null  
                  constraint pk_BooksAuthors_id primary key (BooksAuthors_id)
				  constraint chk_BooksAuthors_id check ([BooksAuthors_id]>=1),
[ISBN]            varchar(100) not null
                  constraint fk_ISBN foreign key references [Books]([ISBN]),
[Author_Id]       int not null
                  constraint fk_Author_Id foreign key references [Authors]([Author_id]),
[Seq_No]          int not null,
[inserted]        datetime not null,
[inserted_by]     varchar(100) not null,
[updated]         datetime null,
[updated_by]      varchar(100)  null
)
go

create table [dbo].[Publishers]
(
[Publisher_Id] int not null IDENTITY 
               constraint pk_Publisher_Id primary key (Publisher_Id),
[Name]         varchar(250) not null
               constraint uniq_Nme unique ([NAme]),
[URL]          varchar(250) not null,
[inserted]     datetime not null,
[inserted_by]  varchar(100) not null,
[updated]      datetime null,
[updated_by]   varchar(100)  null
)
go

create table [dbo].[Authors_log]
(
[operation_id]       int not null IDENTITY (1,1)
                     constraint pk_operation_id primary key (operation_id),
[Author_Id_new]      int null,
[Name_new]           varchar(250) null,
[URL_new]            varchar(250) null,
[Author_Id_old]      int null,
[Name_old]           varchar(250) null,
[URL_old]            varchar(250)  null,
[operation type]     varchar(20) null,
[operation_datetime] datetime
)
go

---- add foreign key fk_Publisher_Id 
alter table [dbo].[Books]
add constraint fk_Publisher_Id
foreign key ([Publisher_Id]) REFERENCES [dbo].[Publishers]([Publisher_Id])
go