﻿use V_Ilkiv_Library
go
---------------------------------------------
select * from V_Ilkiv_Library.dbo.Authors 
inner join V_Ilkiv_Library.dbo.Authors_log on [Name]=V_Ilkiv_Library.dbo.Authors_log.Name_old
go
select * from V_Ilkiv_Library.dbo.Authors_log
go

insert into V_Ilkiv_Library.dbo.Authors 
([Name], [URL], [inserted], [inserted_by])
values 
( 'AYN RAND' , 'www.AYN_RAND.com' , (GETDATE()), (SYSTEM_USER) ),
( 'ERNEST HEMINGWAY' , 'www.ERNEST_HEMINGWAY.com' , (GETDATE()), (SYSTEM_USER) ),
( 'JOAN DIDION' , 'www.JOAN_DIDION.com' , (GETDATE()), (SYSTEM_USER) ),
( 'RAY BRADBURY' , 'www.RAY_BRADBURY.com' , (GETDATE()), (SYSTEM_USER) ),
( 'GEORGE R.R. MARTIN' , 'www.GEORGE_MARTIN.com' , (GETDATE()), (SYSTEM_USER) ),
( 'GILLIAN FLYNN' , 'www.GILLIAN_FLYNN.com' , (GETDATE()), (SYSTEM_USER) ),
( 'VLADIMIR NABOKOV' , 'www.VLADIMIR_NABOKOV.com' , (GETDATE()), (SYSTEM_USER) ),
( 'JANE AUSTEN' , 'www.JANE_AUSTEN.com' , (GETDATE()), (SYSTEM_USER) ),
( 'MARK TWAIN' , 'www.MARK_TWAIN.com' , (GETDATE()), (SYSTEM_USER) ),
( 'MEG WOLITZER' , 'www.MEG_WOLITZER.com' , (GETDATE()), (SYSTEM_USER) ),
( 'ERIK LARSON' , 'www.ERIK_LARSON.com' , (GETDATE()), (SYSTEM_USER) ),
( 'F. SCOTT FITZGERALD' , 'www.F.SCOTT_FITZGERALD.com' , (GETDATE()), (SYSTEM_USER) ),
( 'EDWIDGE DANTICAT' , 'www.EDWIDGE_DANTICAT.com' , (GETDATE()), (SYSTEM_USER) ),
( 'SAMUEL BECKETT' , 'www.SAMUEL_BECKETT.com' , (GETDATE()), (SYSTEM_USER) ),
( 'R.L. STINE' , 'www.R.L._STINE.com' , (GETDATE()), (SYSTEM_USER) ),
( 'AMY TAN' , 'www.AMY_TAN.com' , (GETDATE()), (SYSTEM_USER) ),
( 'J.K. ROWLING' , 'www.J.K._ROWLING.com' , (GETDATE()), (SYSTEM_USER) ),
( 'MAYA ANGELOU' , 'www.MAYA_ANGELOU.com' , (GETDATE()), (SYSTEM_USER) ),
( 'LYDIA DAVIS' , 'www.LYDIA_DAVIS.com' , (GETDATE()), (SYSTEM_USER) ),
( 'HENRY MILLER' , 'www.HENRY_MILLER.com' , (GETDATE()), (SYSTEM_USER) )
go
select * from V_Ilkiv_Library.dbo.Authors 
inner join V_Ilkiv_Library.dbo.Authors_log on [Name]=V_Ilkiv_Library.dbo.Authors_log.Name_new
go
select * from V_Ilkiv_Library.dbo.Authors_log
go

-------------------------------------------------------------------

select * from V_Ilkiv_Library.dbo.Publishers
go
select * from V_Ilkiv_Library.dbo.Authors_log
go

insert into V_Ilkiv_Library.dbo.Publishers
([Name] ,[URL] ,[inserted]  ,[inserted_by])
values
( 'Merwin-Webster' , 'www.Merwin-Webster.com' , (GETDATE()), (SYSTEM_USER) ),
( 'Dubliners' , 'www.Dubliners.com' , (GETDATE()), (SYSTEM_USER) ),
( 'The Paris Review' , 'www.The_Paris_Review.com' , (GETDATE()), (SYSTEM_USER) ),
( 'Barnes & Noble' , 'www.Barnes_&_Noble.com' , (GETDATE()), (SYSTEM_USER) ),
( 'St. John Mandel' , 'www.St.JohnMandel.com' , (GETDATE()), (SYSTEM_USER) ),
( '2014 Reddit AMA' , 'www.2014RedditAMA.com' , (GETDATE()), (SYSTEM_USER) ),
( 'The Metamorphosis' , 'www.TheMetamorphosis.com' , (GETDATE()), (SYSTEM_USER) ),
( 'Olimpe and Theophile' , 'www.Olimpe_and_Theophile.com' , (GETDATE()), (SYSTEM_USER) ),
( 'The French Revolution' , 'www.TheFrenchRevolution.com' , (GETDATE()), (SYSTEM_USER) ),
( 'Old Filth' , 'www.Old_Filth.com' , (GETDATE()), (SYSTEM_USER) ),
( 'The Devil in the White City' , 'www.The_Devil_in_the_White_City.com' , (GETDATE()), (SYSTEM_USER) ),
( 'North Carolina' , 'www.North_Carolina.com' , (GETDATE()), (SYSTEM_USER) ),
( 'Vieux-Chauvet' , 'www.Vieux-Chauvet.com' , (GETDATE()), (SYSTEM_USER) ),
( 'Around the World' , 'www.Around_the_World.com' , (GETDATE()), (SYSTEM_USER) ),
( 'The Washington Post' , 'www.The_Washington_Post.com' , (GETDATE()), (SYSTEM_USER) ),
( 'The Plum in the Golden Vase' , 'www.The_Plum_in_the_Golden_Vase.com' , (GETDATE()), (SYSTEM_USER) ),
( 'Virginia Woolf said of Austen' , 'www.Virginia_Woolf_said_of_Austen.com' , (GETDATE()), (SYSTEM_USER) ),
( 'Little Women' , 'www.Little_Women.com' , (GETDATE()), (SYSTEM_USER) ),
( 'Orient Express' , 'www.Orient-Express.com' , (GETDATE()), (SYSTEM_USER) ),
( 'The Tropic of Cancer' , 'www.The_Tropic_of_Cancer' , (GETDATE()), (SYSTEM_USER) )
go

select * from V_Ilkiv_Library.dbo.Publishers
go
select * from V_Ilkiv_Library.dbo.Authors_log
go

--------------------------------------------------------------------------

select * from V_Ilkiv_Library.dbo.Books
go
select * from V_Ilkiv_Library.dbo.Authors_log
go

insert into V_Ilkiv_Library.dbo.Books
([ISBN] ,[Publisher_Id] ,[URL]  ,[Price], [inserted], [inserted_by])
values
( '1512657841593', 1, 'www.Merwin-Webster.com', 112.5, (GETDATE()), (SYSTEM_USER)),
( '3625487512369', 2, 'www.Dubliners.com', 36.9, (GETDATE()), (SYSTEM_USER)),
( '5841236587412', 3, 'www.The_Paris_Review.com', 45, (GETDATE()), (SYSTEM_USER)),
( '3659874125896', 4, 'www.Barnes_&_Noble.com', 86.2, (GETDATE()), (SYSTEM_USER)),
( '6256215521455', 5, 'www.St.JohnMandel.com', 98.3, (GETDATE()), (SYSTEM_USER)),
( '9658742136585', 6, 'www.2014RedditAMA.com', 25, (GETDATE()), (SYSTEM_USER)),
( '8425658445555', 7, 'www.TheMetamorphosis.com', 52, (GETDATE()), (SYSTEM_USER)),
( '3265987123355', 8, 'www.Olimpe_and_Theophile.com', 74, (GETDATE()), (SYSTEM_USER)),
( '9658362145478', 9, 'www.TheFrenchRevolution.com', 96, (GETDATE()), (SYSTEM_USER)),
( '3659842257474', 10, 'www.Old_Filth.com', 22.9, (GETDATE()), (SYSTEM_USER)),
( '9557452587143', 11, 'www.The_Devil_in_the_White_City.com', 43, (GETDATE()), (SYSTEM_USER)),
( '9314855215556', 12, 'www.North_Carolina.com', 71, (GETDATE()), (SYSTEM_USER)),
( '9515687453663', 13, 'www.Vieux-Chauvet.com', 63, (GETDATE()), (SYSTEM_USER)),
( '3214785699331', 14, 'www.Around_the_World.com', 57, (GETDATE()), (SYSTEM_USER)),
( '3258745696332', 15, 'www.The_Washington_Post.com', 42.5, (GETDATE()), (SYSTEM_USER)),
( '3699742658842', 16, 'www.The_Plum_in_the_Golden_Vase.com', 14.9, (GETDATE()), (SYSTEM_USER)),
( '3699742569874', 17, 'www.Virginia_Woolf_said_of_Austen.com', 41, (GETDATE()), (SYSTEM_USER)),
( '6984236985223', 18, 'www.Little_Women.com', 100, (GETDATE()), (SYSTEM_USER)),
( '3698741258842', 19, 'www.Orient-Express.com', 25, (GETDATE()), (SYSTEM_USER)),
( '3698523698526', 20, 'www.The_Tropic_of_Cancer', 86, (GETDATE()), (SYSTEM_USER))
go

select * from V_Ilkiv_Library.dbo.Books
go
select * from V_Ilkiv_Library.dbo.Authors_log
go

-------------------------------------------------------------------

select * from V_Ilkiv_Library.dbo.BooksAuthors
go
select * from V_Ilkiv_Library.dbo.Authors_log
go

insert into V_Ilkiv_Library.dbo.BooksAuthors
([BooksAuthors_id], [ISBN] ,[Author_Id] ,[Seq_No]  , [inserted], [inserted_by])
values
( 1, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=1), 1, 1, (GETDATE()), (SYSTEM_USER)),
( 2, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=2), 2, 2, (GETDATE()), (SYSTEM_USER)),
( 3, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=3), 3, 3, (GETDATE()), (SYSTEM_USER)),
( 4, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=4), 4, 4, (GETDATE()), (SYSTEM_USER)),
( 5, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=5), 5, 5, (GETDATE()), (SYSTEM_USER)),
( 6, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=6), 6, 6, (GETDATE()), (SYSTEM_USER)),
( 7, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=7), 7, 7, (GETDATE()), (SYSTEM_USER)),
( 8, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=8), 8, 8, (GETDATE()), (SYSTEM_USER)),
( 9, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=9), 9, 9, (GETDATE()), (SYSTEM_USER)),
( 10, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=10), 10, 10, (GETDATE()), (SYSTEM_USER)),
( 11, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=11), 11, 11, (GETDATE()), (SYSTEM_USER)),
( 12, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=12), 12, 12, (GETDATE()), (SYSTEM_USER)),
( 13, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=13), 13, 13, (GETDATE()), (SYSTEM_USER)),
( 14, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=14), 14, 14, (GETDATE()), (SYSTEM_USER)),
( 15, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=15), 15, 15, (GETDATE()), (SYSTEM_USER)),
( 16, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=16), 16, 16, (GETDATE()), (SYSTEM_USER)),
( 17, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=17), 17, 17, (GETDATE()), (SYSTEM_USER)),
( 18, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=18), 18, 18, (GETDATE()), (SYSTEM_USER)),
( 19, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=19), 19, 19, (GETDATE()), (SYSTEM_USER)),
( 20, (select [ISBN] from V_Ilkiv_Library.dbo.Books where V_Ilkiv_Library.dbo.Books.Publisher_Id=20), 20, 20, (GETDATE()), (SYSTEM_USER))
go

select * from V_Ilkiv_Library.dbo.BooksAuthors
go

select * from V_Ilkiv_Library.dbo.Authors_log
go