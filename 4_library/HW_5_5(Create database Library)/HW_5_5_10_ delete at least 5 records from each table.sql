﻿use V_Ilkiv_Library
go
--------------------------------
delete from V_Ilkiv_Library.dbo.BooksAuthors
where BooksAuthors_Id>=15 and BooksAuthors_Id<=20
go
--------------------------------
delete from V_Ilkiv_Library.dbo.Authors
where Author_Id>=15 and Author_Id<=20
go
--------------------------------
delete from V_Ilkiv_Library.dbo.Books
where Publisher_Id>=15 and Publisher_Id<=20
go
--------------------------------
delete from V_Ilkiv_Library.dbo.Publishers
where Publisher_Id>=15 and Publisher_Id<=20
go
--------------------------------
delete from V_Ilkiv_Library.dbo.Authors_log
where operation_id>=15 and operation_id<=20
go

---in last Quety(Authors_log) we have Messages:
---Records can not be deleted
---Msg 3609, Level 16, State 1, Line 15
---The transaction ended in the trigger. The batch has been aborted.
---if we want to delete the lines, we first need to disable the trigger([Authors_log_DEL]), then execute the request and then turn on the trigger back

