﻿use V_Ilkiv_Library
go

create or alter trigger dbo.Authors_log_DEL on V_Ilkiv_Library.[dbo].[Authors_log]
instead of delete 
as
print N'Records can not be deleted'
rollback transaction
return
go




DELETE FROM V_Ilkiv_Library.[dbo].[Authors_log] WHERE [operation_id] = 1;


---we have Messages:
---Records can not be deleted
---Msg 3609, Level 16, State 1, Line 15
---The transaction ended in the trigger. The batch has been aborted.
