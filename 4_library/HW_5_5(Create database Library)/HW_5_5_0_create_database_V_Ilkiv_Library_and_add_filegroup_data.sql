﻿use master
go
drop database if exists [V_Ilkiv_Library]
go
create database [V_Ilkiv_Library]
go
USE [master]
GO
ALTER DATABASE [V_Ilkiv_Library] ADD FILEGROUP [Data]
GO
---------create new data file 
USE [master]
GO
ALTER DATABASE [V_Ilkiv_Library] 
ADD FILE ( NAME = N'V_Ilkiv_Library_data', 
    FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\V_Ilkiv_Library_data.mdf' , 
    SIZE = 5120KB , FILEGROWTH = 1024KB ) TO FILEGROUP [Data]
GO

----------set default file group 
USE [V_Ilkiv_Library]
GO
IF NOT EXISTS (SELECT name FROM sys.filegroups WHERE is_default=1 AND name = N'Data') 
	ALTER DATABASE [V_Ilkiv_Library] MODIFY FILEGROUP [Data] DEFAULT
GO
