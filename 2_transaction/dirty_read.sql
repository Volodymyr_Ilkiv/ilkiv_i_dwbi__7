﻿use Produckt_Storage
go

select * from Produckt_Storage.dbo.goods
go

insert into Produckt_Storage.dbo.goods ([id], [name], [price])
values (1, 'orbit', 20), (2, 'dirol', 22)
go

select * from Produckt_Storage.dbo.goods
go

--------- update transaction
begin transaction
update Produckt_Storage.dbo.goods set Produckt_Storage.dbo.goods.[price] = 15
where Produckt_Storage.dbo.goods.[id] = 1
waitfor delay '00:00:10'
rollback transaction
go

select * from Produckt_Storage.dbo.goods
go

set transaction isolation level read uncommitted
go

begin transaction
select * from [goods]
commit transaction
go