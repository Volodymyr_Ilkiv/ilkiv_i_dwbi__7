﻿use Produckt_Storage
go

begin transaction
select * from goods
waitfor delay '00:00:10'
select * from goods
commit transaction


update goods set [name] = 'Wrigly' where id = 1

begin transaction
	update goods
	set [name] = 'orbit'
	where id = 1
commit transaction