﻿use Produckt_Storage
go






begin transaction
insert into [goods] (id, [name], [price]) values (3, 'Bubelgum', 11)
commit transaction
go



begin transaction
select * from goods where id > 0
waitfor delay '00:00:10'
select * from goods where id > 0
commit transaction

delete from goods
where id = 3

select * from goods where id > 0
go