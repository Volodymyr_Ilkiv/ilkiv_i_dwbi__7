﻿use labor_sql
go
---1
select [maker], [type], [speed], [hd] 
from product
inner join pc on pc.model=product.model
where hd<=8
;

---2
select distinct [maker] 
from product
inner join  pc on pc.model=product.model
where pc.speed>=600
;

---3
select distinct  [maker] 
from product
inner join  laptop on laptop.model=product.model
where laptop.speed<=500
;

---4
select distinct t1.[model], t2.[model], t1.[hd], t1.[ram]
from laptop t1, laptop t2
where t1.hd=t2.hd and t1.ram=t2.ram 
;

---5
select t1.[country], t1.[class] as bb_class, t2.[class] as bc_class
from classes t1, classes t2
where t1.country=t2.country and (t1.type='bb' and t2.type='bc')
;

---6
select pc.[model], [maker]
from pc
inner join product on product.model=pc.model
where price<600
;

---7
select printer.[model], [maker]
from printer
inner join product on product.model=printer.model
where price>300
;

---8
select  [maker], product.[model], [price]
from pc
inner join product on product.model=pc.model
--where 
;

---9
select  [maker], product.[model], [price]
from pc
inner join product on product.model=pc.model
where product.type='pc' --and (pc.price not null)
;

---10
select  [maker], [type], product.[model], [speed]
from laptop
inner join product on product.model=laptop.model
where speed>600
;

---11
select [name], [displacement]
from ships
inner join classes on classes.class=ships.class
;

---12
select [ship], [name], [date]
from battles
inner join outcomes on outcomes.battle=battles.name
where result !='sunk'
;

---13
select [name], [country]
from ships
inner join classes on classes.class=ships.class
;

---14
select distinct [plane], [name]
from company
inner join trip on trip.id_comp=company.id_comp
where plane='Boeing'
;

---15
select [name], [date]
from passenger
inner join pass_in_trip on pass_in_trip.id_psg=passenger.id_psg
;

---16
select product.[model], [speed], [hd]
from pc
inner join product on product.model=pc.model
where  hd=10 or hd=20 and maker='A' 
;

---17
select *
from product
pivot (
count([model]) for [type] in ([pc], [laptop], [printer])
) as PVTtable1
;

---18
select [avg_], [11], [12], [14], [15]
from (select 'Averege prace' as avg_, screen, price from laptop) z
pivot (
avg([price]) for screen in ([11], [12], [14], [15])
) as PVTtable2
;

---19
select [maker], l.[model]
from laptop l
cross apply product
where product.model=l.model
;


---20
select lap.*, z.[max_price]
from laptop lap
inner join product x on x.model=lap.model
cross apply (select [maker], MAX([price]) as max_price from laptop l
inner join product on product.model=l.model
GROUP BY product.maker) z
where x.maker=z.maker
order by code
;

---21
select *
from laptop lap1
cross apply(
select top 1 * from laptop lap2
where lap1.model<lap2.model or (lap1.model=lap2.model and lap1.code<lap2.code)
order by model, code) z
order by lap1.model
;

---22
select *
from laptop lap1
outer apply(
select top 1 *from laptop lap2
where lap1.model<lap2.model or (lap1.model=lap2.model and lap1.code<lap2.code)
order by model, code) z
order by lap1.model
;

---23
select z.* 
from (select distinct type from product) prod1
cross apply(
select top 3 * from product prod2
where prod1.type=prod2.type
order by prod2.model
) z
;

---24
select code, name, value from laptop
cross apply
    (values('speed', speed),
           ('ram', ram),
           ('hd', hd),
           ('screen', screen)
    ) z(name, value)
order by code, name, value;