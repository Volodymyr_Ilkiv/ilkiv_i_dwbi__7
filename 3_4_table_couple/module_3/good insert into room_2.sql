﻿use V_I_module_3
go

insert into [V_I_module_3].[dbo].[room]
(
       
       [number]
      ,[type]
      ,[status]
      ,[creation_date]
      ,[comment]
      ,[balcony]
      ,[bed]
      ,[extra_bed]
      ,[is_use]
)
values
(
 
       11
      ,'suite'
      ,'on sale'
      ,'2018-06-01 12:00:00'
      ,'big balcony'
      ,1
      ,2
      ,3
      ,1
)
go

select * from [dbo].[room]
go