﻿use V_I_module_3
go

insert into [V_I_module_3].[dbo].[room]
(
       
       [number]
      ,[type]
      ,[status]
      ,[creation_date]
      ,[comment]
      ,[balcony]
      ,[bed]
      ,[extra_bed]
      ,[is_use]
)
values
(
 
       10
      ,'classick'
      ,'on sale'
      ,'2018-01-01 00:00:00'
      ,'good room'
      ,null
      ,2
      ,1
      ,1
)
go

select * from [dbo].[room]
go