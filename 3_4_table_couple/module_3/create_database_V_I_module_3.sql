﻿use master
go

drop database if exists [V_I_module_3]
go

create database [V_I_module_3]
go

use [V_I_module_3]
go

drop table if exists [dbo].[room]
go

create table [dbo].[room]
(
[id]            int not null IDENTITY (1 , 1) ,
[number]        int not null,
[type]          varchar(20) not null,
[status]        varchar(20) not null,
[creation_date] datetime not null,
[comment]       varchar(250) not null,
[balcony]       int,
[bed]           int not null,
[extra_bed]     int
constraint [check_extra_bed] check ([extra_bed]<=3) ,
[is_use]        bit,
CONSTRAINT [PK_goods] PRIMARY KEY CLUSTERED ([id] ASC),
constraint [number_room] unique ([number])
);
go

drop table if exists [dbo].[guest]
go

create table [dbo].[guest]
(
[id]             int not null IDENTITY (1, 1),
[name]           varchar(250) not null,
[room_id]        int not null,
[arrival_date]   datetime,
[departure_time] datetime,
[email]          varchar(100) not null
constraint [check_email] check ([email] like '%@%'),
[phone_number]   varchar(50) not null,
[children]       int,
[insert_date]    datetime,
[update_date]    datetime,
constraint [PK_guest] primary key clustered (id),
constraint [phone_guest] unique ([phone_number]),
constraint [FK_room_id] foreign key ([room_id]) references [dbo].[room]([id])
);
go