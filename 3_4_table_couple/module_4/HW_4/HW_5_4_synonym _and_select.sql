﻿use education
go 

drop schema if exists [V_ilkiv]
go

create schema [V_ilkiv]
go

drop synonym if exists [V_ilkiv].[produckt]
go

drop synonym if exists [V_ilkiv].[produckt_operation]
go

create synonym [V_ilkiv].[produckt] for [V_I_module_3].[mod].[produckt]
go

create synonym [V_ilkiv].[produckt_operation] for [V_I_module_3].[mod].[produckt_operation]
go

select * from [V_ilkiv].[produckt]
go

select * from [V_ilkiv].[produckt_operation]
go

select [name], price, [provider] from [V_ilkiv].[produckt]
go

select [name], price, [provider], [type_operation], [date_operation] from  [V_ilkiv].[produckt_operation]
go