﻿use V_I_module_3
go

insert into mod.produckt
(
       [name]
      ,[size]
      ,[quantity]
      ,[price]
      ,[total_price]
      ,[provider]
      ,[provider_phone]
      ,[provideer_email]
      ,[price_in_valute]
      ,[manufacturer]
      ,[country_manufacturer]
      ,[sold]
      ,[tax_type]
      ,[tax_rare]
      ,[sum_tax_rare]
	  )
	  values 
	  ('Xiaomi Redmi 4', '15*20*10', 15, 4200, 63000, 'Hongong company LTD', 
	  '+247964587123', 'sales@mi.com', 159.36, 'Xiaomi company', 'China',
	  1,  1, 0.20, 840),
	  ('Samsung Galaxy 9', '25*20*15', 10, 29000, 290000, 'Samsung company', 
	  '+693874521458', 'sales@samsung.com', 1065.59, 'Samsung company', 'Japan',
	  1,  1, 0.20, 5800),
	  ('Huawei Honor P20', '15*25*10', 8, 12530, 100240, 'Huawei company LTD', 
	  '+247964587123', 'sales@huawei.com', 398.75, 'Huawei company', 'China',
	  1,  1, 0.20, 2506),
	  ('LG G6', '15*20*10', 3, 15000, 45000, 'LG company ', 
	  '+693254789623', 'sales@lg.com', 625, 'LG company', 'China',
	  1,  1, 0.20, 3000)
	  go

	  select * from mod.produckt
	  go

	  select * from mod.produckt_operation
	  go