﻿use V_I_module_3
go

drop trigger if exists  [guest.tr_update]
go

create trigger [guest.tr_update] on [dbo].[guest]
after update
as
begin
update V_I_module_3.dbo.guest
set update_date=(GETDATE())
where V_I_module_3.dbo.guest.id in (select id from inserted)

end

go