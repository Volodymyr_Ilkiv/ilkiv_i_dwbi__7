﻿Use V_I_module_3
go

--- create views
CREATE OR ALTER VIEW [info_guest] 
(
       [guest_name]
      ,[arrival_date]
      ,[departure_time]
      ,[guest_email]
	  ,[guest_phone_number]
	  ,[quantity_children]
  )
  AS
  SELECT 
       [name]
      ,[arrival_date]
      ,[departure_time]
      ,[email]
	  ,[phone_number]
	  ,[children]
  FROM [dbo].[guest]
go

CREATE OR ALTER VIEW [info_room] 
(
       [room_number]
      ,[room_type]
      ,[quantity_balcony]
      ,[quantity_bed]
	  ,[quantity_extra_bed]
	  ,[comment]
  )
  AS
  SELECT 
       [number]
      ,[type]
      ,[balcony]
      ,[bed]
	  ,[extra_bed]
	  ,[comment]
  FROM [dbo].[room]
go

select * from [info_guest] 
go

select * from [info_room] 
go