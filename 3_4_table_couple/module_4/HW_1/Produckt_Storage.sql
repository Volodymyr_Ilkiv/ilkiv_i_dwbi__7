﻿

DROP TABLE [dbo].[goods_in_overhead];
GO


DROP TABLE [dbo].[overhead];
GO


DROP TABLE [dbo].[storage];
GO


DROP TABLE [dbo].[provider];
GO


DROP TABLE [dbo].[goods];
GO


DROP TABLE [dbo].[tare];
GO


--************************************** [dbo].[provider]

CREATE TABLE [dbo].[provider]
(
 [id]           INT NOT NULL ,
 [phone_number] VARCHAR(15) NOT NULL ,
 [manager_name] VARBINARY(50) NOT NULL ,

 CONSTRAINT [PK_provider] PRIMARY KEY CLUSTERED ([id] ASC)
);
GO



--************************************** [dbo].[goods]

CREATE TABLE [dbo].[goods]
(
 [id]                 INT NOT NULL ,
 [name]               VARCHAR(50) NOT NULL ,
 [image_goods]        IMAGE NULL ,
 [nutritional_values] TEXT NULL ,
 [price]              DECIMAL(19,4) NOT NULL ,

 CONSTRAINT [PK_goods] PRIMARY KEY CLUSTERED ([id] ASC)
);
GO



--************************************** [dbo].[tare]

CREATE TABLE [dbo].[tare]
(
 [id]   INT NOT NULL ,
 [type] VARCHAR(20) NOT NULL ,

 CONSTRAINT [PK_tare] PRIMARY KEY CLUSTERED ([id] ASC)
);
GO



--************************************** [dbo].[overhead]

CREATE TABLE [dbo].[overhead]
(
 [id]            INT NOT NULL ,
 [provider_id]   INT NOT NULL ,
 [overhead_date] DATE NOT NULL ,
 [overhead_sum]  DECIMAL(19,4) NOT NULL ,

 CONSTRAINT [PK_overhead] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [FK_71] FOREIGN KEY ([provider_id])
  REFERENCES [dbo].[provider]([id])
);
GO


--SKIP Index: [fkIdx_71]


--************************************** [dbo].[storage]

CREATE TABLE [dbo].[storage]
(
 [id]        INT NOT NULL ,
 [goods_id]  INT NOT NULL ,
 [tare_id]   INT NOT NULL ,
 [amount]    INT NOT NULL ,
 [total_sum] DECIMAL(19,4) NOT NULL ,

 CONSTRAINT [PK_storage] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [FK_51] FOREIGN KEY ([goods_id])
  REFERENCES [dbo].[goods]([id]),
 CONSTRAINT [FK_55] FOREIGN KEY ([tare_id])
  REFERENCES [dbo].[tare]([id])
);
GO


--SKIP Index: [fkIdx_51]

--SKIP Index: [fkIdx_55]


--************************************** [dbo].[goods_in_overhead]

CREATE TABLE [dbo].[goods_in_overhead]
(
 [id]          INT NOT NULL ,
 [overhead_id] INT NOT NULL ,
 [goods_id]    INT NOT NULL ,
 [amount]      INT NOT NULL ,
 [price]       DECIMAL(18,4) NOT NULL ,
 [total_price] DECIMAL(18,4) NOT NULL ,

 CONSTRAINT [PK_goods_in_overhead] PRIMARY KEY CLUSTERED ([id] ASC),
 CONSTRAINT [FK_81] FOREIGN KEY ([overhead_id])
  REFERENCES [dbo].[overhead]([id]),
 CONSTRAINT [FK_85] FOREIGN KEY ([goods_id])
  REFERENCES [dbo].[goods]([id])
);
GO


--SKIP Index: [fkIdx_81]

--SKIP Index: [fkIdx_85]


