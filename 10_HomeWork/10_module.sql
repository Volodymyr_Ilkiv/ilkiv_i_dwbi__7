﻿use labor_sql
go

---1
select ROW_NUMBER() OVER(ORDER BY id_comp) AS num, trip_no, id_comp
from trip
group by  id_comp, trip_no
;

---2

select ROW_NUMBER() OVER(ORDER BY id_comp) AS num, trip_no, id_comp
from trip
group by  id_comp, trip_no
;

---3
;WITH printer_cte
AS
(
SELECT model, color, [type], price, 
     RANK() OVER(PARTITION BY [type] ORDER BY price) AS price_rank
FROM printer
)
SELECT model, color, [type], price
FROM printer_cte
WHERE price_rank=1
;

---4
;WITH pc_cte
AS
(
SELECT maker, ROW_NUMBER() OVER (PARTITION BY maker ORDER BY model) AS num_model
FROM product
WHERE [type]='pc'
)
SELECT DISTINCT maker
FROM pc_cte
WHERE num_model>2
;

---5
; with price_cte as 
(select price ,ROW_NUMBER() OVER(ORDER BY price) AS row_number_
from pc)
select price from price_cte
where row_number_=(select COUNT(row_number_)-1 from price_cte)
;

---6
;WITH pass_cte 
AS
(
SELECT id_psg, [name],
	  ltrim(reverse(substring(ltrim(REVERSE([name])),1,CHARINDEX(' ',ltrim(REVERSE([name])))))) as surname
FROM passenger
)
SELECT id_psg, [name], 
       NTILE(3) OVER(ORDER BY surname) AS passenger_group 
FROM pass_cte
ORDER BY surname
;

---7

SELECT code, model, speed, ram, hd, cd, price,
   ROW_NUMBER() OVER(ORDER BY price DESC) AS id,
   COUNT(*) OVER() AS row_total,
   NTILE(CAST((SELECT CEILING(COUNT(*)/3.0) FROM PC) AS bigint)) OVER(ORDER BY price DESC) AS page_num,
	   (SELECT CEILING(COUNT(*)/3.0) FROM PC) AS page_total
FROM pc
ORDER BY price DESC
;

---8
;WITH union_cte
AS
(
SELECT [out] AS [sum], 'out' AS [type], [date], point
FROM outcome
UNION ALL
SELECT [out], 'out' AS [type], [date], point
FROM outcome_o
UNION ALL
SELECT inc, 'inc' AS [type], [date], point
FROM income
UNION ALL
SELECT inc, 'inc' AS [type], [date], point
FROM income_o
),
max_sum_cte
AS
(
SELECT [sum], [type], [date], point,
 RANK() OVER(ORDER BY [sum] DESC) AS sum_rank
FROM union_cte
)
SELECT [sum] AS max_sum, [type], [date], point
FROM max_sum_cte
WHERE sum_rank=1
;


---9
;WITH pc_cte
AS
(
SELECT code, model, speed, ram, hd, cd, price,
	   AVG(price) OVER(PARTITION BY speed) AS local_price,
	   AVG(price) OVER() AS total_price
FROM pc
)
SELECT code, model, speed, ram, hd, cd, price,
   price-local_price AS df_local_prica,
   price-total_price AS df_total_price,
   total_price
FROM pc_cte
;

